from rest_framework import serializers
from users.models import User
from django.contrib.auth.models import Group

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    class Meta:
        model = User
        fields = ('phone_number', 'email', 'name', 'groups',)

    def to_representation(self, obj):
        serialized_data = super(UserSerializer, self).to_representation(obj)
        user = None
        request = self.context.get("request")

        if not request and not hasattr(request, "user"):
            serialized_data.pop('groups')

        return serialized_data
