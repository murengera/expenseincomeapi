from  django.urls import path

from . import  views

urlpatterns=[
    path('income',views.IncomeListAPIView.as_view(),name="incomes"),
    path('income/<int:id>', views.IncomeDetailAPIView.as_view(), name="expenses"),
]