from django.core.mail import EmailMessage
from .utils import sms_backend
from celeryconfig import app
from users.models import User

@app.task
def send_sms_action(message, phone_numbers):
    print("SENDING SMS {}".format(message))

    if phone_numbers is not None and len(phone_numbers) > 0:
        try:
            sms_backend.send(message=message, recipients=phone_numbers, sender_id="expe-income")
        except:
            raise Exception("Can't send sms to all provided phone numbers")




@app.task
def send_email_action(subject, message, user=None, email=None):
    if user:
        user = User.objects.filter(id=user).first()
    print("SENDING SMS")

    if subject is not None and message is not None:
        if user is None and email is None:
            pass
        elif user is not None:
            em = EmailMessage(
                subject, message, to=[user.email])
            em.send()
        elif email is not None:
            em = EmailMessage(
                subject, message, to=[email])
            em.send()
    else:
        raise Exception("Email content is not valid")
